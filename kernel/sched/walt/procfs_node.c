// SPDX-License-Identifier: GPL-2.0-only
/* sysfs_node.c
 *
 * Android Vendor Hook Support
 *
 * Copyright 2020 Google LLC
 */
#include <linux/lockdep.h>
#include <linux/kobject.h>
#include <linux/sched.h>
#include <linux/sched/signal.h>
#include <linux/sched/task.h>
#include <linux/proc_fs.h>
#include <linux/uaccess.h>
#include <linux/sched.h>
#include <trace/events/power.h>

#include "walt.h"
#include "trace.h"

#define MAX_PROC_SIZE 128

static const char *GRP_NAME[TASK_GROUP_MAX] = {"sys", "ta", "fg", "cam", "bg", "sys_bg",
				       "nnapi", "dex2oat"};

#define PROC_OPS_RW(__name) \
		static int __name##_proc_open(\
			struct inode *inode, struct file *file) \
		{ \
			return single_open(file,\
			__name##_show, PDE_DATA(inode));\
		} \
		static const struct proc_ops  __name##_proc_ops = { \
			.proc_open	=  __name##_proc_open, \
			.proc_read	= seq_read, \
			.proc_lseek	= seq_lseek,\
			.proc_release = single_release,\
			.proc_write	=  __name##_store,\
		}

#define PROC_OPS_RO(__name) \
		static int __name##_proc_open(\
			struct inode *inode, struct file *file) \
		{ \
			return single_open(file,\
			__name##_show, PDE_DATA(inode));\
		} \
		static const struct proc_ops __name##_proc_ops = { \
			.proc_open	= __name##_proc_open, \
			.proc_read	= seq_read, \
			.proc_lseek	= seq_lseek,\
			.proc_release = single_release,\
		}

#define PROC_OPS_WO(__name) \
		static int __name##_proc_open(\
			struct inode *inode, struct file *file) \
		{ \
			return single_open(file,\
			NULL, NULL);\
		} \
		static const struct proc_ops __name##_proc_ops = { \
			.proc_open	= __name##_proc_open, \
			.proc_lseek	= seq_lseek,\
			.proc_release = single_release,\
			.proc_write	= __name##_store,\
		}

#define PROC_ENTRY(__name) {__stringify(__name), &__name##_proc_ops}

#define SET_VENDOR_GROUP_STORE(__grp, __vg)						      \
		static ssize_t set_task_group_##__grp##_store(struct file *filp, \
			const char __user *ubuf, \
			size_t count, loff_t *pos) \
		{									      \
			char buf[MAX_PROC_SIZE];	\
			int ret;   \
			if (count >= sizeof(buf))	\
				return -EINVAL;	\
			if (copy_from_user(buf, ubuf, count))	\
				return -EFAULT;	\
			buf[count] = '\0';	\
			ret = update_vendor_group_attribute(buf, __vg);   \
			return ret ?: count;						      \
		}									      \
		PROC_OPS_WO(set_task_group_##__grp);

/// ******************************************************************************** ///
/// ********************* New code section ***************************************** ///
/// ******************************************************************************** ///
static int dump_task_show(struct seq_file *m, void *v)
{									      
	struct task_struct *p, *t;
	int pid, group_leader_pid, real_parent_pid, rt;
	enum walt_oem_task_group group;
	const char *grp_name = "unknown";

	rcu_read_lock();

	for_each_process_thread(p, t) {
		get_task_struct(t);
		group = walt_get_task_group(t);
		if (group >= 0 && group < TASK_GROUP_MAX)
			grp_name = GRP_NAME[group];
		rt = walt_rt_task(t);
		pid = t->pid;
		group_leader_pid = t->group_leader ? t->group_leader->pid : -1;
		real_parent_pid = t->real_parent ? t->real_parent->pid : -1;
		put_task_struct(t);
		seq_printf(m, "%u %s %s %u %i %i", pid, t->comm, grp_name, rt, group_leader_pid, real_parent_pid);
		seq_printf(m, "\n");
	}

	rcu_read_unlock();

	return 0;
}
PROC_OPS_RO(dump_task);

static int version_show(struct seq_file *m, void *v)
{
	seq_printf(m, "You are running on // WALT Plus //\n");
	seq_printf(m, "A UI-oriented scheduler based on Qualcomm WALT\n");
	seq_printf(m, "(C) 2021 - Qualcomm Innovation Center (QUIC)\n");
	seq_printf(m, "(C) 2023 - TenSeventy7\n");
	seq_printf(m, "https://tenseventyseven.cf\n\n");
	seq_printf(m, "Support? https://github.com/Mint-Kernel/kernel-msm/issues\n");

	return 0;
}
PROC_OPS_RO(version);

struct pentry {
	const char *name;
	const struct proc_ops *fops;
};
static struct pentry entries[] = {
	// Vendor group attributes
	PROC_ENTRY(dump_task),
	PROC_ENTRY(version),
};

struct proc_dir_entry *walt_plus_proc;
int create_procfs_node(void)
{
	int i;

	walt_plus_proc = proc_mkdir("walt_plus", NULL);

	if (!walt_plus_proc)
		goto out;

	/* create procfs */
	for (i = 0; i < ARRAY_SIZE(entries); i++) {
		umode_t mode;

		if (entries[i].fops->proc_write == NULL) {
			mode = 0444;
		} else if(entries[i].fops->proc_read== NULL) {
			mode = 0200;
		} else {
			mode = 0644;
		}

		if (!proc_create(entries[i].name, mode,
					walt_plus_proc, entries[i].fops)) {
			pr_debug("%s(), create %s failed\n",
					__func__, entries[i].name);
			remove_proc_entry("walt_plus", NULL);

			goto out;
		}
	}

	return 0;

out:
	return -ENOMEM;
}
